# cls-adaptive

## Project setup
```
* npm install
* npm install -g json-server
```

### Compiles and hot-reloads for development
```
* npm run serve
* json-server --watch ./src/data/db.json
* json-server --watch ./src/data/userData.json --port 3001
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).