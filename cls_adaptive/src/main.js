import Vue from 'vue'
import VueX from 'vuex'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import CountryFlag from 'vue-country-flag'
import HistogramSlider from "vue-histogram-slider";
import "vue-histogram-slider/dist/histogram-slider.css";

Vue.component(HistogramSlider.name, HistogramSlider); 
Vue.component('country-flag', CountryFlag)

Vue.config.productionTip = false

Vue.use(VueX)


const store = new VueX.Store({
  state: {    
    userID: 0,
    token: "empty",
    permission: 0,
    lastGroup: 0
  },
  mutations: {
    setOwnderID (state, id) {
      state.userID = id
    },
    setToken (state, token) {
      state.token = token
    },
    setPermission (state, permission) {
      state.permission = permission
    },
    setLastGroup (state, group) {
      state.lastGroup = group
    }
  }
})

//store.commit('setOwnderID', 12)

new Vue({
  store: store,
  vuetify,
  render: h => h(App)
}).$mount('#app')