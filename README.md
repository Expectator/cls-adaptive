# CLS-adaptive

## Host through CMD

### System requirements
Only cmd/shell ist needed to run CLS-adaptive on the local machine.

### Setup for loaclhost


#### Launch app

To run cls-adaptive enter the commands in cmd/shell navigate to the directories:

cls_adaptive directory:
$npm run serve

cls-backend directory:
$npm run start

The applications starts and can be found at localhost/8080

## Host through Docker

### System requirements
Only `docker` is needed to run CLS-adaptive on the local machine. You can find installation instructions here:

https://www.docker.com/products

To run cls-adaptive under `Windows`, docker must be allowed to access the host system. If not, cls-adaptive will not be able to access the database which is stored and mounted on the host drive from docker-compose.

The datbase has to be stored on your system, because when you rebuild the containers of docker-compose, all of your collected data would be deleted.

https://docs.docker.com/docker-for-windows/troubleshoot/#volume-mounting-requires-shared-drives-for-linux-containers

### Setup for localhost


#### Launch app
To launch the app after you completed the system requirements, just do the following steps:

* open your favorite command line with docker access (`docker --version` to check)
* navigate to the directory where the unzipped files of `cls-adaptive.zip` are (`docker-compose.yml` should be in there)
* Type following command to start cls-adaptiv:
 
  `docker-compose up --build -d` (this may take a while at the first build)

* you can check the running containers with `docker ps`
* now open your webbrowser and type in `http://localhost` as your url
* press enter and cls-adaptive start-page should be in front of your eyes

#### Stop app
To stop the app just enter the following commands:

* `docker-compose down`
* enter `docker ps` to check if the containers stopped


## Questions

If something doesn't work correctly or you just want to ask for an issue, feel free to send an e-mail.


