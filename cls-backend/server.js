/*
This class holds all the logic of the server.
An Expresse App is created to make REST requests to the server.
Every request is defined by a list url ending of the requests and holds database logic.
If the sqlite database has not been generated create the database upoon start of the server.
*/
var express = require("express"),
    mailer = require('express-mailer');
var cors = require('cors')
var app = express()
var db = require("./database.js")
var bodyParser = require("body-parser");
var bcrypt = require('bcryptjs');
var pug = require('pug');

//activate cors to enable requests
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//create mailer to send emails
mailer.extend(app, {
    from: 'cls.adaptive@gmail.com',
    host: 'smtp.gmail.com', // hostname
    secureConnection: true, // use SSL
    port: 465, // port for secure SMTP
    transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
    auth: {
        user: 'cls.adaptive@gmail.com',
        pass: 'drgDFHDFHsd'
    }
});

//set directory of email templates
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');

//set cors listen for default request
app.listen(8000, function () {
    console.log('CORS-enabled web server listening on port 8000')
})

//set oringin for cors request
/*
var corsOptions = {
    origin: 'localhost',
    optionsSuccessStatus: 200
  }
*/

// GET hash key for new account
app.get("/newAccount", (req, res, next) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var length = 32;
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    res.json({
        "value": result
    });
});

// GET list of all previous groups
app.get("/groupList", (req, res, next) => {
    var sql = "select * from groupList"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "value": rows
        })
    });
});

// GET list of all previous groups
app.get("/groupListDates/:groupID", (req, res, next) => {
    var sql = "select start, end from groupList WHERE id = ?"
    var params = [req.params.groupID]
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "value": rows[0]
        })
    });
});

// GET last used groupID
app.get("/lastID/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;
                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "select id from groupList ORDER BY id DESC LIMIT 1"
                            var params = []
                            db.all(sql, params, (err, rows) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message });
                                    return;
                                }
                                res.json({
                                    "value": rows[0]
                                })
                            });
                        });
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

// GET all items
app.get("/items", (req, res, next) => {
    var sql = "select * from items"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "item": rows
        })
    });
});

// GET all breakValues
app.get("/breakValues", (req, res, next) => {
    var sql = "select * from breakValues"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "item": rows
        })
    });
});

// GET all item difficulties
app.get("/itemDifficulties", (req, res, next) => {
    var sql = "select difficulty from items"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "item": rows
        })
    });
});

// GET all new items
app.get("/newItems", (req, res, next) => {
    var sql = "select * from newItems"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "item": rows
        })
    });
});

// GET all new items answers
app.get("/newItemsAnswers/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1 && req.params.token.length == 64) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "select * from newItemsAnswers"
                            var params = []
                            db.all(sql, params, (err, rows) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message });
                                    return;
                                }
                                res.json({
                                    "item": rows
                                })
                            });
                        });
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})


// GET all items answers
app.get("/itemsAnswers/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1 && req.params.token.length == 64) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "select * from itemsAnswers"
                            var params = []
                            db.all(sql, params, (err, rows) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message });
                                    return;
                                }
                                res.json({
                                    "item": rows
                                })
                            });
                        });
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

// GET all groupLinks
app.get("/groupLinks", (req, res, next) => {
    var sql = "select * from groupLinks"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "groups": rows
        })
    });
});

// GET last groupLink id
app.get("/groupLinks/lastID/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;
                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "select COUNT(*) from groupLinks"
                            var params = []
                            db.all(sql, params, (err, rows) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message });
                                    return;
                                }
                                res.json({
                                    "groupID": rows[0]
                                })
                            });
                        });
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

// GET the userTemplate
app.get("/userTemplate", (req, res, next) => {
    var sql = "select * from userTemplate"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "user": rows[0]
        })
    });
});

// GET the userData
app.get("/userData/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;
                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "select * from userData"
                            var params = []
                            db.all(sql, params, (err, rows) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message });
                                    return;
                                }
                                res.json({
                                    "users": rows
                                })
                            });
                        });
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

// GET the userData for specific account
app.get("/userData/:groupID/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;
                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "select * from userData WHERE groupID = ?"
                            var params = [req.params.groupID]
                            db.all(sql, params, (err, rows) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message });
                                    return;
                                }
                                res.json({
                                    "data": rows
                                })
                            });
                        });
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

// GET the userData
app.get("/userData", (req, res, next) => {
    var sql = "select * from userData"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "users": rows
        })
    });
})

// GET the grouplist for specific user
app.get("/groupList/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "select * from groupList WHERE groupOwnerID = ?"
                            var params = [req.params.user]
                            db.all(sql, params, (err, rows) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message });
                                    return;
                                }
                                res.json({
                                    "users": rows
                                })
                            });
                        });
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });

                }
            })
        }
    })
})

// GET the userData final difficulty
app.get("/userData/final_Difficulty", (req, res, next) => {
    var sql = "select final_Difficulty from userData"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "users": rows
        })
    });
});

// GET the accounts
app.get("/accounts/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);
        if (exist == 1 && req.params.token.length == 64) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "select * from accounts"
                            var params = []
                            db.all(sql, params, (err, rows) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message });
                                    return;
                                }
                                res.json({
                                    "user": rows
                                })
                            });
                        });
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });

                }
            })
        }
    })
})

// GET the tokens
app.get("/tokens", (req, res, next) => {
    var sql = "select * from tokens"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "user": rows
        })
    });
});

// GET the identification
app.get("/identification", (req, res, next) => {
    var sql = "select * from identification"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "data": rows
        })
    });
});

// POST request to send group participation notifcation
app.post("/emailNotification/:address/:identification/:feedback/:groupID/:modules/:language/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var address = req.params.address
                            var shortAddress = address.slice(0, 3)
                            var language = req.params.language
                            var subj = "";
                            var template = "";
                            if (language == "GER") {
                                subj = "cls-adaptive Teilnahme Einladung";
                                template = "notificationGer";
                            } else {
                                subj = "cls-adaptive pariticipation invitation";
                                template = "notificationEng"
                            }
                            var link = "";
                            if (req.params.identification == 4) {
                                link = "http://localhost:8080/?" + req.params.identification + "?" + req.params.feedback + "?" + req.params.groupID + "?" + req.params.modules
                            } else {
                                link = "http://localhost:8080/?" + req.params.identification + "?" + req.params.feedback + "?" + req.params.groupID + "?" + req.params.modules + "?" + shortAddress
                            }
                            app.mailer.send(template, {
                                to: address, // REQUIRED. This can be a comma delimited string just like a normal email to field. 
                                subject: subj, // REQUIRED.
                                otherProperty: link // All additional properties are also passed to the template as local variables.
                            }, function (err) {
                                if (err) {
                                    // handle error
                                    console.log(err);
                                    res.send('There was an error sending the email');
                                    return;
                                }
                                res.send('Email Sent');
                            });
                        });
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

//POST to send password reset email to user
app.post("/emailReset/:address/:language", (req, res, next) => {
    var address = req.params.address
    var language = req.params.language
    var subj = "";
    var template = "";
    if (language == "GER") {
        subj = "cls-adaptive Passwort ändern";
        template = "passwordResetGer";
    } else {
        subj = "cls-adaptive change password";
        template = "passwordResetEng"
    }
    var link = "http://localhost:8080/?newPassword?" + req.params.address
    app.mailer.send(template, {
        to: address, // REQUIRED. This can be a comma delimited string just like a normal email to field. 
        subject: subj, // REQUIRED.
        otherProperty: link // All additional properties are also passed to the template as local variables.
    }, function (err) {
        if (err) {
            // handle error
            console.log(err);
            res.send('There was an error sending the email');
            return;
        }
        res.send('Email Sent');
    });
});

// Post specific account login
app.post("/accounts/:user", (req, res, next) => {
    var data = {
        username: req.body.user,
        password: req.body.pw
    }

    var check = "SELECT EXISTS(SELECT * from accounts WHERE username = ?)"
    var checkParams = [req.params.user]
    db.all(check, checkParams, (err, result) => {
        var exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);
        if (exist == 1) {
            var sql = "SELECT * from accounts WHERE username = ?"
            var params = [req.params.user]
            db.all(sql, params, (err, row) => {
                if (err) {
                    res.status(400).json({ "error": err.message });
                    return;
                } else {
                    if (row[0].permission != 0) {
                        var permission = row[0].permission;
                        var pw = row[0].password;
                        var encryptor = require("simple-encryptor")(row[0].key);
                        var hash = encryptor.decrypt(pw);
                        var passwordHashed = data.password;
                        if (bcrypt.compareSync(hash, passwordHashed) === true) {
                            var time = new Date();
                            var sql = "INSERT INTO tokens (token, userID, timeout) VALUES (?,?,?)"

                            var result = '';
                            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                            if(permission == 1) {
                                var length = 32;
                            } else {
                                var length = 64;
                            }                                                        
                            var charactersLength = characters.length;
                            for (var i = 0; i < length; i++) {
                                result += characters.charAt(Math.floor(Math.random() * charactersLength));
                            }

                            var params = [result, row[0].id, time]
                            db.run(sql, params)

                            var tmp = [row[0].id, row[0].permission];

                            res.json({
                                data: tmp, result
                            })
                        } else {
                            res.json({
                                data: "false"
                            })
                        }
                    } else {
                        res.json({
                            data: "false"
                        })
                    }
                }
            });
        } else {
            res.json({
                data: "false"
            })
        }
    })
});

// POST new group, checks for valid token, rejects request if token invalid
app.post("/groupList", (req, res, next) => {
    var data = {
        groupOwnerID: req.body.groupOwnerID,
        groupName: req.body.groupName,
        groupShort: req.body.groupShort,
        identification: req.body.identification,
        feedback: req.body.feedback,
        link: req.body.link,
        created: req.body.created,
        start: req.body.start,
        end: req.body.end,
        experience: req.body.experience,
        usecases: req.body.usecases,
        userInformation: req.body.userInformation,
        token: req.body.token
    }

    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [data.groupOwnerID, data.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [data.groupOwnerID, data.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            var participants = 0;
                            var sql = "INSERT INTO groupList (groupOwnerID, groupName, groupShort, identification, feedback, link, participants, created, start, end, experience, usecases, userInformation) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"
                            var params = [data.groupOwnerID, data.groupName, data.groupShort, data.identification, data.feedback, data.link, participants, data.created, data.start, data.end, data.experience, data.usecases, data.userInformation]
                            db.run(sql, params, (err, result) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message })
                                    console.log("hit4")
                                    return;
                                }
                                res.json({
                                    "data": "success"
                                })
                            });
                        });
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });

                }
            })
        }
    })
})

// POST new cls-userData
app.post("/userData", (req, res, next) => {
    var data = {
        firstUsed: req.body.firstUsed,
        useTime: req.body.useTime,
        gender: req.body.gender,
        age: req.body.age,
        final_Difficulty: req.body.final_Difficulty,
        time: req.body.time,
        groupID: req.body.groupID,
        identificationNumber: req.body.identificationNumber,
        identificationMail: req.body.identificationMail,
        textediting: req.body.textediting,
        spreadsheets: req.body.spreadsheets,
        presentation: req.body.presentation,
        pictureediting: req.body.pictureediting,
        coding: req.body.coding,
        gaming: req.body.gaming,
        email: req.body.email,
        internetsurfing: req.body.internetsurfing,
        informationgathering: req.body.informationgathering,
        onlineshopping: req.body.onlineshopping,
        onlinebanking: req.body.onlineshopping
    }
    var sql = "INSERT INTO userData (firstUsed, useTime, gender, age, final_Difficulty, time, groupID, identificationNumber, identificationMail, textediting, spreadsheets, presentation, pictureediting, coding, gaming, email, internetsurfing, informationgathering, onlineshopping, onlinebanking) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
    var params = [data.firstUsed, data.useTime, data.gender, data.age, data.final_Difficulty, data.time, data.groupID, data.identificationNumber, data.identificationMail, data.textediting, data.spreadsheets, data.presentation, data.pictureediting, data.coding, data.gaming, data.email, data.internetsurfing, data.informationgathering, data.onlineshopping, data.onlinebanking]
    db.run(sql, params, (err, result) => {
        if (err) {
            res.status(400).json({ "error": err.message })
            return;
        }
        res.json({
            "data": data
        })
    });
});

// POST item
app.post("/items/:user/:token", (req, res, next) => {
    var data = {
        itemID: req.body.itemID,
        difficulty: req.body.difficulty,
        question: req.body.question,
        target: req.body.target,
        groupID: req.body.groupID,
        symbol: req.body.symbol
    }
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1 && req.params.token.length == 64) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "INSERT INTO items (itemID, difficulty, question, target, groupID, symbol) VALUES (?,?,?,?,?,?)"
                            var params = [data.itemID, data.difficulty, data.question, data.target, data.groupID, data.symbol]
                            db.run(sql, params, (err, result) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message })
                                    return;
                                }
                                res.json({
                                    "data": data
                                })
                            });
                        })
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

// POST breakValues
app.post("/breakValues/:user/:token", (req, res, next) => {
    var data = {
        breakValue: req.body.breakValue
    }
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1 && req.params.token.length == 64) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "INSERT INTO breakValues (breakValue) VALUES (?)"
                            var params = [data.breakValue]
                            db.run(sql, params, (err, result) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message })
                                    return;
                                }
                                res.json({
                                    "data": data
                                })
                            });
                        })
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})


// POST new item
app.post("/newItems/:user/:token", (req, res, next) => {
    var data = {
        question: req.body.question,
        target: req.body.target,
        groupID: req.body.groupID,
        symbol: req.body.symbol
    }
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1 && req.params.token.length == 64) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "INSERT INTO newItems (question, target, groupID, symbol) VALUES (?,?,?,?)"
                            var params = [data.question, data.target, data.groupID, data.symbol]
                            db.run(sql, params, (err, result) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message })
                                    return;
                                }
                                res.json({
                                    "data": data
                                })
                            });
                        })
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

// POST new item answer
app.post("/newItemsAnswers", (req, res, next) => {
    var data = {
        itemID: req.body.itemID,
        answer: req.body.answer,
        finalDifficulty: req.body.finalDifficulty
    }
    var sql = "INSERT INTO newItemsAnswers (itemID, answer, finalDifficulty) VALUES (?,?,?)"
    var params = [data.itemID, data.answer, data.finalDifficulty]
    db.run(sql, params, (err, result) => {
        if (err) {
            res.status(400).json({ "error": err.message })
            return;
        }
        res.json({
            "data": data
        })
    });
})

// POST item answer
app.post("/itemsAnswers", (req, res, next) => {
    var data = {
        itemID: req.body.itemID,
        answer: req.body.answer,
        finalDifficulty: req.body.finalDifficulty
    }
    var sql = "INSERT INTO itemsAnswers (itemID, answer, finalDifficulty) VALUES (?,?,?)"
    var params = [data.itemID, data.answer, data.finalDifficulty]
    db.run(sql, params, (err, result) => {
        if (err) {
            res.status(400).json({ "error": err.message })
            return;
        }
        res.json({
            "data": data
        })
    });
})

// POST new groupLink
app.post("/groupLinks/:user/:token", (req, res, next) => {
    var data = {
        groupID: req.body.groupID,
        target0: req.body.target0,
        target1: req.body.target1,
        target2: req.body.target2,
        target3: req.body.target3,
        target4: req.body.target4,
        target5: req.body.target5,
        target6: req.body.target6,
        target7: req.body.target7
    }
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1 && req.params.token.length == 64) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            var sql = "INSERT INTO groupLinks (groupID, target0, target1, target2, target3, target4, target5, target6, target7) VALUES (?,?,?,?,?,?,?,?,?)"
                            var params = [data.groupID, data.target0, data.target1, data.target2, data.target3, data.target4, data.target5, data.target6, data.target7]
                            db.run(sql, params, (err, result) => {
                                if (err) {
                                    res.status(400).json({ "error": err.message })
                                    return;
                                }
                                res.json({
                                    "data": data
                                })
                            });
                        })
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})


// POST account
app.post("/accounts", (req, res, next) => {
    var data = {
        username: req.body.username,
        password: req.body.password,
        key: req.body.key,
        language: req.body.language
    }
    var exists = "SELECT EXISTS(SELECT * from accounts WHERE username = ?)"
    var existParams = [data.username]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 0) {
            var permission = 0;
            var sql = "INSERT INTO accounts (username, password, key, permission) VALUES (?,?,?,?)"
            var params = [data.username, data.password, data.key, permission]
            db.run(sql, params, (err, result) => {
                if (err) {
                    res.status(400).json({ "error": err.message })
                    return;
                }
                emailRegistration(data.username, data.key, data.language);
                res.json({
                    "data": data
                })
            });
        } else {
            res.json({
                data: "false"
            })
        }
    })
})

// POST identification custom id
app.post("/identification", (req, res, next) => {
    var data = {
        user: req.body.user,
        token: req.body.token,
        userID: req.body.userID,
        groupID: req.body.groupID
    }

    var check = "SELECT EXISTS(SELECT * from identification WHERE userID = ? AND groupID = ?)"
    var checkParams = [req.body.userID, req.body.groupID]
    db.all(check, checkParams, (err, result) => {
        var exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);
        if (exist == 0) {
            var sql = "INSERT INTO identification (userID, groupID) VALUES (?,?)"
            var params = [data.userID, data.groupID]
            db.run(sql, params, (err, result) => {
                if (err) {
                    res.status(400).json({ "error": err.message })
                    return;
                }
                res.json({
                    "data": data
                })
            });
        } else {
            res.json({
                data: "false"
            })
        }
    })
})

// PATCH identification generated id
app.patch("/identificationGenerated", (req, res, next) => {
    var data = {
        groupID: req.body.groupID
    }

    var check = "SELECT EXISTS(SELECT * from identification WHERE groupID = ?)"
    var checkParams = [req.body.groupID]
    db.all(check, checkParams, (err, result) => {
        var exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);
        if (exist == 1) {
            db.run('UPDATE identification SET userID = userID + 1 WHERE groupID = ?',
                [data.groupID],
                (err, result) => {
                    if (err) {
                        res.status(400).json({ "error": res.message })
                        return;
                    }
                    var sql = "select * from identification WHERE groupID = ?"
                    var params = [data.groupID]
                    db.all(sql, params, (err, rows) => {
                        if (err) {
                            res.status(400).json({ "error": err.message });
                            return;
                        }
                        res.json({
                            data: rows[0].userID
                        })
                    });
                })
        }
    })
})

// PATCH items
app.patch("/items/patch/:itemID", (req, res, next) => {
    var data = {
        itemID: req.body.itemID,
        difficulty: req.body.difficulty,
        question: req.body.question,
        target: req.body.target,
        groupID: req.body.groupID,
        symbol: req.body.symbol
    }
    db.run(
        `UPDATE items set
           itemID = COALESCE(?,itemID),
           difficulty = COALESCE(?,difficulty),
           question = COALESCE(?,question),
           target = COALESCE(?,target),
           groupID = COALESCE(?,groupID),
           symbol = COALESCE(?,symbol)
           WHERE itemID = ?`,
        [data.itemID, data.difficulty, data.question, data.target, data.groupID, data.symbol, req.params.itemID],
        (err, result) => {
            if (err) {
                res.status(400).json({ "error": res.message })
                return;
            }
            res.json({
                message: "success",
                data: data,
                changes: this.changes
            })
        });
})

// PATCH token
app.patch("/tokens/patch/:userID", (req, res, next) => {
    var time = new Date();
    db.run(
        `UPDATE tokens SET
            timeout = COALESCE(?,timeout)
            WHERE userID = ?`,
        [time, req.params.userID],
        (err, result) => {
            if (err) {
                res.status(400).json({ "error": res.message })
                return;
            }
            res.json({
                message: "success",
                changes: this.changes
            })
        });
})

// PATCH participants in group
app.patch("/groupParticipants/:groupID", (req, res, next) => {
    db.run(
        `UPDATE groupList SET
            participants = participants + 1
            WHERE id = ?`,
        [req.params.groupID],
        (err, result) => {
            if (err) {
                res.status(400).json({ "error": res.message })
                return;
            }
            res.json({
                message: "success",
                //data: data,
                changes: this.changes
            })
        });
})

// PATCH permission of user
app.patch("/accountsPermission/:userID/:level/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1 && req.params.token.length == 64) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            db.run(
                                `UPDATE accounts SET
                                permission = ?
                                WHERE id = ?`,
                                [req.params.level, req.params.userID],
                                (err, result) => {
                                    if (err) {
                                        res.status(400).json({ "error": res.message })
                                        return;
                                    }
                                    res.json({
                                        message: "success",
                                        //data: data,
                                        changes: this.changes
                                    })
                                });
                        })
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})


// PATCH permission of user
app.patch("/accountActivation/:userID/:key", (req, res, next) => {
    db.run(
        `UPDATE accounts SET
            permission = 1
            WHERE id = ? AND key = ?`,
        [req.params.userID, req.params.key],
        (err, result) => {
            if (err) {
                res.status(400).json({ "error": res.message })
                return;
            }
            res.json({
                message: "success",
                //data: data,
                changes: this.changes
            })
        });
})

// PATCH password of user
app.patch("/passwordReset/:username/:password/:key", (req, res, next) => {
    db.run(
        `UPDATE accounts SET
            password = ?,
            key = ?
            WHERE username = ?`,
        [req.params.password, req.params.key, req.params.username],
        (err, result) => {
            if (err) {
                res.status(400).json({ "error": res.message })
                return;
            }
            res.json({
                message: "success"
            })
        });
})

// PATCH end of test group
app.patch("/groupList/end/:end/:groupID/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;
                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            db.run(
                                `UPDATE groupList SET
                                end = ?
                                WHERE id = ?`,
                                [req.params.end, req.params.groupID],
                                (err, result) => {
                                    if (err) {
                                        res.status(400).json({ "error": res.message })
                                        return;
                                    }
                                    res.json({
                                        message: "success"
                                    })
                                });
                        });
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

//delete token
app.delete("/token/:id", (req, res, next) => {
    db.run(
        'DELETE FROM tokens WHERE userID = ?',
        req.params.id,
        function (err, result) {
            if (err) {
                res.status(400).json({ "error": res.message })
                return;
            }
            res.json({ "message": "deleted", changes: this.changes })
        });
})

//clear brake values table
app.delete("/breakValues/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1 && req.params.token.length == 64) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;
                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            db.run(
                                'DELETE FROM breakValues',
                                function (err, result) {
                                    if (err) {
                                        res.status(400).json({ "error": res.message })
                                        return;
                                    }
                                    res.json({ "message": "deleted", changes: this.changes })
                                });
                        })
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

//delete user
app.delete("/accounts/:userID/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1 && req.params.token.length == 64) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;
                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            db.run(
                                'DELETE FROM accounts WHERE id = ?',
                                req.params.userID,
                                function (err, result) {
                                    if (err) {
                                        res.status(400).json({ "error": res.message })
                                        return;
                                    }
                                    res.json({ "message": "deleted", changes: this.changes })
                                });
                        })
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

//delete group
app.delete("/groupList/delete/:groupID/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;
                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            db.run(
                                'DELETE FROM groupList WHERE id = ?',
                                req.params.groupID,
                                function (err, result) {
                                    if (err) {
                                        res.status(400).json({ "error": res.message })
                                        return;
                                    }
                                    res.json({ "message": "deleted", changes: this.changes })
                                });
                        })
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})

//delete newItem
app.delete("/newItems/:itemID/:user/:token", (req, res, next) => {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [req.params.user, req.params.token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1 && req.params.token.length == 64) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [req.params.user, req.params.token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }

                            db.run(
                                'DELETE FROM newItems WHERE id = ?',
                                req.params.itemID,
                                function (err, result) {
                                    if (err) {
                                        res.status(400).json({ "error": res.message })
                                        return;
                                    }
                                    res.json({ "message": "deleted", changes: this.changes })
                                });
                        })
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            if (err) {
                                res.status(400).json({ "error": res.message })
                                return;
                            }
                            res.json({
                                "data": "error"
                            })
                        });
                }
            })
        }
    })
})


// Default response for any other request
app.use(function (req, res) {
    res.status(404)
});

function emailRegistration(email, key, language) {
    var address = email;
    var exists = "SELECT * FROM accounts WHERE username = ?"
    var existParams = [email]
    var subj = ""
    var template = "";
    if (language == "GER") {
        subj = "cls-adaptive Registrierung";
        template = "registrationGer";
    } else {
        subj = "cls-adaptive registration";
        template = "registrationEng";
    }
    db.all(exists, existParams, (err, result) => {
        var id = result[0].id
        //console.log(id)
        var link = "http://localhost:8080/?emailRegistration?" + id + "?" + key;
        app.mailer.send(template, {
            to: address, // REQUIRED. This can be a comma delimited string just like a normal email to field. 
            subject: subj, // REQUIRED.
            otherProperty: link // All additional properties are also passed to the template as local variables.
        }, function (err) {
            if (err) {
                // handle error
                console.log(err);
                //res.send('There was an error sending the email');
                return;
            }
        }
        )
    })
};

function checkToken(userID, token) {
    var tokenAccepted = 0;
    var exist = 0;
    var exists = "SELECT EXISTS(SELECT * from tokens WHERE userID = ? AND token = ?)"
    var existParams = [userID, token]
    db.all(exists, existParams, (err, result) => {
        exist = JSON.stringify(result[0]);
        exist = exist.slice(-2);
        exist = exist.slice(0, 1);

        if (exist == 1) {
            var check = "SELECT * from tokens WHERE userID = ? AND token = ?"
            var checkParams = [userID, token]
            db.all(check, checkParams, (err, rows) => {
                if (err) {
                    tokenAccepted = false;
                    return false;
                }
                var compareDate = new Date();
                if (rows[0].timeout > compareDate - 1800000) {
                    tokenAccepted = true;

                    db.run(
                        'UPDATE tokens SET timeout = ? WHERE token = ?',
                        [compareDate, rows[0].token],
                        (err, result) => {
                            if (err) {
                                return false
                            } else {
                                console.log("hit4")
                                return true;
                            }
                            //res.status(400).json({ "error": res.message })                            
                        })
                } else {
                    db.run(
                        'DELETE FROM tokens WHERE token = ?',
                        rows[0].token,
                        function (err, result) {
                            /*if (err) {
                                //res.status(400).json({ "error": res.message })
                                return false;
                            }*/
                            /*res.json({
                                "data": "error"
                            })*/
                            if (err) {
                                return false;
                            } else {
                                return false;
                            }
                        });
                }
            })
        }
    })
}